#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/time.h>
#include <fcntl.h>

#include <iostream>
#include "opencv2/opencv.hpp"
#include <opencv2/core/types.hpp>

using namespace cv;
using namespace std;

#define CAMERA_W_SET 320
#define CAMERA_H_SET 240
uint32_t CAMERA_W_ACTUAL=CAMERA_W_SET;
uint32_t CAMERA_H_ACTUAL=CAMERA_H_SET;

struct frame_info // 结构体 用于存储帧数据
{
    int x;
    int y;
    int w;
    int h;
    int time; // 开始时间
};

int g_tcp_fd;
bool g_flag_start=false;
int g_frame_index=0;
struct frame_info g_frame_info_list[400];

///////////////////////////////////////////////////////////////
// lw_h2f_axi bus
#define LW_BRIDGE_BASE		0xFF200000
#define LW_BRIDGE_SPAN		0x00005000

// IP Base Addresses
// D8M Frame Diff SubSystem
#define CAPTURE_RESET_N_BASE                0x00000410
#define CAPTURE_0_VIP_CAPTURE_0_BASE        0x00000420
#define DIFF_0_TERASIC_FRAME_DIFF_BASE      0x00000440

// buffer
#define MAGIC_NUM 'K'

#define CMD_BUF1_PADDR  _IOR(MAGIC_NUM,0,int)
#define CMD_BUF2_PADDR  _IOR(MAGIC_NUM,1,int)
#define CMD_SET_READ_BUF     _IOW(MAGIC_NUM,6,int)
#define CMD_SET_WRITE_BUF    _IOW(MAGIC_NUM,7,int)

#define CMD_BUF_ADDR_W0 CMD_BUF1_PADDR
#define CMD_BUF_ADDR_W1 CMD_BUF2_PADDR
#define BUF_W0 1
#define BUF_W1 2

// streaming status for writer
#define ST_STANDY           0
#define ST_WAIT_SOP         1
#define ST_PROCESS          2
#define ST_SUCCESS          3
#define ST_FIFO_OVERFLOW    4
#define ST_BAD_FRAME        5
#define ST_FC_FRAME_EOF     6

///////////////////////////////////////////////////////////////////////////////////////////

// Frame Diff SubSystem
// FPGA processing  architecture
static volatile unsigned long *g_h2f_ip_frame_diff_addr=NULL;  // IMAGE Process  Diff + threshold
static volatile unsigned long *g_h2f_ip_capture_reset_n_addr=NULL;
static volatile unsigned long *g_h2f_ip_capture_vip_capture_0_addr=NULL;

////////////////////////////////////////////
//
int g_buffer_fd;

struct buffer_info // 结构体 buffer调度
{
    bool     ready; // Linux Side;
    bool     idle; //released by FPGA or Linux;
    int      index; // index of dma buffers (linux driver side view)
    uint32_t phy_addr; // physical address
};

buffer_info g_ddr3_write_buffer[2];

///////////////////////////////////////////////////////////////////////////////////////////
//  FPGA  IP Control
int FPGA_IPBaseInit(){
	int fd;
	void *lw_axi_virtual_base;
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( -1 );
	}
	lw_axi_virtual_base = mmap( NULL, LW_BRIDGE_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, LW_BRIDGE_BASE );
	if( lw_axi_virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap() failed...\n" );
		close( fd );
		return( -1 );
	}

    // Frame Diff SubSystem
    g_h2f_ip_frame_diff_addr                = ( unsigned long  *)(lw_axi_virtual_base + DIFF_0_TERASIC_FRAME_DIFF_BASE);
    g_h2f_ip_capture_reset_n_addr           = ( unsigned long  *)(lw_axi_virtual_base + CAPTURE_RESET_N_BASE);
    g_h2f_ip_capture_vip_capture_0_addr     = ( unsigned long  *)(lw_axi_virtual_base + CAPTURE_0_VIP_CAPTURE_0_BASE );

    close( fd );
    return 0;
}

//  Diff + threshold
void FPGA_FrameDiff_ImageProcess(uint8_t threshold){
    g_h2f_ip_frame_diff_addr[0x01]=threshold;// Threshold
}

// D8M Capture SubSystem  IP Functions
void FPGA_Capture_Stop(){
    g_h2f_ip_capture_vip_capture_0_addr[0x00]=0x0000;
    usleep(100);
}
// Start Capture
bool FPGA_Capture_Start(int BufferToDDR3_Num){
    if(BufferToDDR3_Num >= 2 || BufferToDDR3_Num <0) {
        printf("[FPGA_Capture_Start] Driver only allocate 2 write buffer\r\n");
        return  false;
    }
    g_h2f_ip_capture_vip_capture_0_addr[0x00]=0x0000;     // stop
    g_h2f_ip_capture_vip_capture_0_addr[0x02]=g_ddr3_write_buffer[BufferToDDR3_Num].phy_addr;    // set base addr
    g_h2f_ip_capture_vip_capture_0_addr[0x00]=0x0001;    // start

    return true;
}

// Check Capture Status
bool FPGA_Capture_Status(int BufferToDDR3_Num, uint32_t  Width, uint32_t  Height,double timeout_ms){
    if(BufferToDDR3_Num >= 2 || BufferToDDR3_Num <0) {
        printf("[FPGA_Capture_Status] Driver only allocate 2 write buffer\r\n");
        return  false;
    }

    bool bSuccess = false;
    uint32_t  Status32, PixelCnt;

    double timeout;
    timeout = (double)getTickCount() + timeout_ms/1000*getTickFrequency();

    do {
        usleep(50);
        Status32=g_h2f_ip_capture_vip_capture_0_addr[0x01];
	    //printf("%d Status32 \n",Status32);
	} while ((Status32 != ST_SUCCESS && Status32 != ST_FIFO_OVERFLOW && Status32 != ST_BAD_FRAME && Status32 != ST_FC_FRAME_EOF)
                && ( (double)getTickCount() < timeout));
    //抓完对Capture 复位一下；
    FPGA_Capture_Stop();
    // printf("[FPGA_Capture_Status] %d Status32 \n",Status32);

    PixelCnt=g_h2f_ip_capture_vip_capture_0_addr[0x03];

    if (Status32 == ST_SUCCESS || Status32 == ST_FC_FRAME_EOF){
        if (PixelCnt == Width*Height){
            // printf("[FPGA_Capture_Status]  %d*%d pixel write \n",Width,Height);
            g_ddr3_write_buffer[BufferToDDR3_Num].idle=false; //这片写buffer 标记为 非IDLE
            g_ddr3_write_buffer[BufferToDDR3_Num].ready=true; //这片写buffer 标记为 Ready
            bSuccess = true;
        }else{
            // printf("[FPGA_Capture_Status] invalid pixel count %d\r\n", (int)PixelCnt);
            bSuccess = false;
        }
    }else{
        // printf("[FPGA_Capture_Status] Status32:%d; invalid pixel count %d\r\n",Status32 ,(int)PixelCnt);
        bSuccess = false;
    }

    if ((double)getTickCount() > timeout){
        bSuccess = false;
        return -1;
    }

    return bSuccess;
}

///////////////////////////////////////////////////////////////////////////////////////////
//  HPS DDR3 Buffer  Memory transfer
//  Write Buffer:  (app side view, for fpga side is read)
//      4 buffer 640x480x4 byte
//      camera -> app -> hps ddr3 -> fpga process
//
// Read Buffer: (app side view, for fpga side is write)
//      2 buffer 640x480x1 byte
//      fpga process -> hps ddr3 buffer -> app


//////////////////////////////////////////////////
// buffer init
bool Buffer_init(){
    g_buffer_fd=open("/dev/dma_buffer",O_RDWR);
    if (g_buffer_fd == -1) {
		printf("[Error] [Buffer_init] open /dev/dma_buffer failed\r\n");
		printf("[Buffer_init] you can try:\r\n");
		printf("        insmod dma_buffer.ko\r\n");
		printf("command first\r\n");
        return false;
    }

    for(int i=0;i<2;i++){
        g_ddr3_write_buffer[i].idle  = true;
        g_ddr3_write_buffer[i].ready = false;
    }

    g_ddr3_write_buffer[0].index = BUF_W0;
    g_ddr3_write_buffer[1].index = BUF_W1;

	uint32_t phy_addr_w0,phy_addr_w1;

	ioctl(g_buffer_fd,CMD_BUF_ADDR_W0,&phy_addr_w0);
	ioctl(g_buffer_fd,CMD_BUF_ADDR_W1,&phy_addr_w1);
	//printf("Write buffer 0 physical address:0x%x\n",phy_addr_w0);
	//printf("Write buffer 1 physical address:0x%x\n",phy_addr_w1);

    g_ddr3_write_buffer[0].phy_addr = phy_addr_w0;
    g_ddr3_write_buffer[1].phy_addr = phy_addr_w1;

    return true;
}

int Buffer_GetWritableIndex(){
    //printf("[Buffer_GetWritableIndex]: idle  ");
    //for(int i=0;i<4;i++)
    //    printf("[%d]:%d;   " ,i,g_ddr3_write_buffer[i].idle);
    //printf("\r\n");

    for(int i=0;i<2;i++){
        if(g_ddr3_write_buffer[i].idle)
            return i;
    }
    return -1;
}

bool HPS_DDR3_BufferReadforDisplay(Mat DstFrame, int BufferToDDR3_Num){
    if(BufferToDDR3_Num >= 2  || BufferToDDR3_Num <0) {
        printf("[HPS_DDR3_BufferReadforDisplay] Driver only allocate 2 write buffer\r\n");
        return  false;
    }
    if(!g_ddr3_write_buffer[BufferToDDR3_Num].ready){
        printf("[HPS_DDR3_BufferReadforDisplay] Display Buffer not Ready??\r\n");
    }
    // Switch to Read Buffer BufferToDDR3_Num
	ioctl(g_buffer_fd,CMD_SET_READ_BUF,g_ddr3_write_buffer[BufferToDDR3_Num].index);

    // copy data from SrcFrame to Buffer
    int retval = read(g_buffer_fd, DstFrame.data, CAMERA_W_ACTUAL*CAMERA_H_ACTUAL*4);  //BGRA
       if (retval == -1) {
        printf("[HPS_DDR3_BufferReadforDisplay] read error\r\n");
        return  false;
      }

    return true;
}

// 计算矩形间的最小距离
// https://blog.csdn.net/DeliaPu/article/details/104837064
int min_distance_of_rectangles(Rect rect1, Rect rect2)
{
	int min_dist;

	//首先计算两个矩形中心点
	Point C1, C2;
	C1.x = rect1.x + (rect1.width / 2);
	C1.y = rect1.y + (rect1.height / 2);
	C2.x = rect2.x + (rect2.width / 2);
	C2.y = rect2.y + (rect2.height / 2);

	// 分别计算两矩形中心点在X轴和Y轴方向的距离
	int Dx, Dy;
	Dx = abs(C2.x - C1.x);
	Dy = abs(C2.y - C1.y);

	//两矩形不相交，在X轴方向有部分重合的两个矩形，最小距离是上矩形的下边线与下矩形的上边线之间的距离
	if((Dx < ((rect1.width + rect2.width)/ 2)) && (Dy >= ((rect1.height + rect2.height) / 2))){
		min_dist = Dy - ((rect1.height + rect2.height) / 2);
	}

	//两矩形不相交，在Y轴方向有部分重合的两个矩形，最小距离是左矩形的右边线与右矩形的左边线之间的距离
	else if((Dx >= ((rect1.width + rect2.width)/ 2)) && (Dy < ((rect1.height + rect2.height) / 2))){
		min_dist = Dx - ((rect1.width + rect2.width)/ 2);
	}

	//两矩形不相交，在X轴和Y轴方向无重合的两个矩形，最小距离是距离最近的两个顶点之间的距离，
	// 利用勾股定理，很容易算出这一距离
	else if((Dx >= ((rect1.width + rect2.width)/ 2)) && (Dy >= ((rect1.height + rect2.height) / 2))){
		int delta_x = Dx - ((rect1.width + rect2.width)/ 2);
		int delta_y = Dy - ((rect1.height + rect2.height)/ 2);
		min_dist = sqrt(delta_x * delta_x  + delta_y * delta_y);
	}

	//两矩形相交，最小距离为负值，返回-1
	else{
		min_dist = -1;
	}

	return min_dist;
}

//将FPGA运算得到的帧间差分法二值图像，进行后处理
void PostProcessingOfOutDiffFrame(Mat Frame){

    medianBlur(Frame,Frame,3);//中值滤波
    // dilate(Frame, Frame,getStructuringElement(MORPH_RECT, Size(5, 5)));//膨胀 5x5

    //将距离较近的 矩形框进行连接合并
    Mat imgtmp;
    Frame.copyTo(imgtmp);
    vector<vector<Point>> contours;
    findContours(imgtmp, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    for (int i = 0; i < contours.size(); i++){
        RotatedRect rect_i=minAreaRect(contours[i]);

        for (int j = 0; j < contours.size(); j++){
            RotatedRect rect_j=minAreaRect(contours[j]);

            if (i!=j){
                Rect r_j = rect_j.boundingRect();
                Rect r_i = rect_i.boundingRect();
                int distance = min_distance_of_rectangles(r_i,r_j);
                if(distance<=(int)(30*CAMERA_W_ACTUAL/320)){
                    //printf("i=%d,j=%d,distance=%d\r\n",i,j,distance);
                    int minx=min(r_i.x,r_j.x);
                    int maxx=max(r_i.x+r_i.width,r_j.x+r_j.width);
                    int miny=min(r_i.y,r_j.y);
                    int maxy=max(r_i.y+r_i.height,r_j.y+r_j.height);
                    rectangle(Frame,Point(minx,miny),Point(maxx,maxy), Scalar(255), -1,1);
                }
            }
        }
    }
    // imshow("medianBlur dilate connect", Frame);
}

//////////////////////////////////////////////////////////////////////////////////

int cvimg2sockdata2(Mat frame, char *buff){
    vector<int> params;
	params.resize(3, 0);
	params[0] = cv::IMWRITE_JPEG_QUALITY;
	params[1] = 50;
    vector<uchar> buf;
	imencode(".jpg", frame, buf, params);

    int nsize = buf.size();
    int i=0;
    for(;i<nsize;i++){
        buff[i]=buf[i];
    }
    return nsize;
}

void deal_with_tcp(){
    struct sockaddr_in client_addr;
    socklen_t length = sizeof(client_addr);
    while(true){
        ///成功返回非负描述字，出错返回-1
        printf("wait for connecting...\n");
        int conn = accept(g_tcp_fd, (struct sockaddr*)&client_addr, &length);
        char buffer[1024];
        while(true){
          memset(buffer, 0 ,sizeof(buffer));
          int len = recv(conn, buffer, sizeof(buffer), 0);
          if(len==0){
            break;
          }
          printf("data:%s\n",buffer);
          if(strcmp(buffer,"get_frame_data")==0){
            //暂停统计
            g_flag_start=false;
            //打包数据并发送
            int length=sizeof(struct frame_info)*g_frame_index;
            printf("length:%d\n",length);
            send(conn,&length,4,0);
            send(conn,g_frame_info_list,length,0);
            // index 清零
            g_frame_index=0;
            memset(g_frame_info_list, 0 ,sizeof(g_frame_info_list));
          }else if(strcmp(buffer,"start")==0){
            g_flag_start=true;
            printf("set g_flag_start true\n");
          }
        }
    }
}

int main(int argc,char *argv[]){
    /* tcp server & udp client */
    int udp_dest_port=0;
    char* udp_dest_ip_address=NULL;

    int tcp_dest_port=0;
    char* tcp_dest_ip_address=NULL;

    // 根据参数选择端口号，设置ip地址
    if(argc==4){
        if(strcmp(argv[1],"A")==0){
            udp_dest_port=10000;
            tcp_dest_port=12345;
        }else if(strcmp(argv[1],"B")==0){
            udp_dest_port=10001;
            tcp_dest_port=12346;
        }
        // 设置IP地址
        udp_dest_ip_address=argv[2];
        tcp_dest_ip_address=argv[3];

        printf("camera type=%s\nudp_server_ip=%s\nudp_server_port=%d\nlocal_tcp_server_ip=%s\ntcp_server_port=%d\n",
                argv[1],udp_dest_ip_address, udp_dest_port, tcp_dest_ip_address, tcp_dest_port);
    }else{
        printf("Usage: ./camera_side_v2 A/B endpoint_ip localhost_ip\n");
        printf("   eg: ./camera_side_v2 A 192.168.1.2 192.168.1.3\n");
        return 0;
    }

    // 定义socket文件描述符
    int udp_sock_fd,tcp_sock_fd;

    // 建立udp & tcp socket
    udp_sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    tcp_sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(udp_sock_fd < 0 || tcp_sock_fd < 0){
        perror("socket");
        exit(1);
    }
    // 将tcp socket 作为全局使用
    g_tcp_fd=tcp_sock_fd;

    // 设置tcp端口号reuse
    int opt=1;
    setsockopt(tcp_sock_fd,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));

    // 填充socket
    struct sockaddr_in udp_addr_serv,tcp_addr_serv;
    memset(&udp_addr_serv, 0, sizeof(udp_addr_serv));
    memset(&tcp_addr_serv, 0, sizeof(tcp_addr_serv));

    udp_addr_serv.sin_family = AF_INET;
    udp_addr_serv.sin_addr.s_addr = inet_addr(udp_dest_ip_address);
    udp_addr_serv.sin_port = htons(udp_dest_port);

    tcp_addr_serv.sin_family = AF_INET;
    tcp_addr_serv.sin_port = htons(tcp_dest_port);
    tcp_addr_serv.sin_addr.s_addr = htonl(INADDR_ANY);

    // 绑定 tcp socket文件描述符 和 socket 信息
    if(bind(tcp_sock_fd, (struct sockaddr* ) &tcp_addr_serv, sizeof(tcp_addr_serv))==-1){
        perror("bind");
        exit(1);
    }
    // 设置监听client个数
    if(listen(tcp_sock_fd, 1) == -1){
        perror("listen");
        exit(1);
    }

    // 创建线程运行 tcp socket 处理函数
    pthread_t thread1;
    int ret_thrd1;
    ret_thrd1 = pthread_create(&thread1, NULL, (void *)&deal_with_tcp, NULL);
    if (ret_thrd1 != 0) {
        printf("线程创建失败\n");
        exit(1);
    }

    /* init diff ip & buffer */

    //初始化 FPGA IP 寄存器控制
    int ret = FPGA_IPBaseInit();
    if(ret==-1){
        printf("init fpga ip base address failed!\n");
        exit(1);
    }
    //初始化 buffer 调度相关
    if(Buffer_init()==false){
        printf("init buffer failed!\n");
        exit(1);
    }

    // set threshold
    FPGA_FrameDiff_ImageProcess(15);

    /* 定义变量 */

    // 存放压缩的图片，用于传输
    char encodeImg[65535];

    // 用于标记是否为start帧
    int is_start_frame = 1;

    // 用于存放buffer索引号，当前buffer和前一个buffer
    int CurNum_w,PreNum_w;

    // 存放当前帧的信息
    struct frame_info pre_frame_info;

    // 用于存放diff 结果
    Mat MemOutFrame;
    MemOutFrame = Mat::zeros(Size(CAMERA_W_ACTUAL,CAMERA_H_ACTUAL), CV_8UC1);

    // 用于图片显示的 BGRA 通道图像
    Mat DisplayFrame,DisplayFrameGray,SendFrame;
    DisplayFrame = Mat::zeros(Size(CAMERA_W_ACTUAL,CAMERA_H_ACTUAL), CV_8UC4 );
    DisplayFrameGray = Mat::zeros(Size(CAMERA_W_ACTUAL,CAMERA_H_ACTUAL), CV_8UC1 );

    // 存放时间信息
    double cur_time,pre_time,start_time;

    // 设置start tick数
    start_time = (double)getTickCount();

    while(true){

        // D8M -> Capture to Buffer  Frame Diff Processing
        // PingPong
        if(is_start_frame == 1){ // 第一帧 发动 抓一帧到 write Buffer
            // 将 frame 图像 送入 HPS DDR3 缓存中
            CurNum_w = Buffer_GetWritableIndex();
            FPGA_Capture_Start(CurNum_w);

            // 记录当前时间
            cur_time=((double)getTickCount() - start_time) / getTickFrequency() ;
            cur_time = cur_time*100000; // 转换为整数便于传输

            // set start frame flag 0
            is_start_frame=0;
        }else {
            //上一帧 D8M -> Buffer是否完成
            if(FPGA_Capture_Status(PreNum_w,CAMERA_W_ACTUAL,CAMERA_H_ACTUAL,60)==false){
                printf("Previous Capture Not Ready!\n");
            }
            // 获取buffer index
            CurNum_w = Buffer_GetWritableIndex();
            // 设置另一个buffer状态为idle
            g_ddr3_write_buffer[(CurNum_w+1)%2].idle = true;
            // 启动Capture
            FPGA_Capture_Start(CurNum_w);
            // 记录时间
            cur_time=((double)getTickCount() - start_time) / getTickFrequency() ;
            cur_time = cur_time*100000;

            // 从buffer读取图像和diff结果
            HPS_DDR3_BufferReadforDisplay(DisplayFrame,PreNum_w);

            // 拆分通道，提取 BGR 和 diff 结果
            Mat channels[4];
            split(DisplayFrame,channels);
            // diff 结果
            MemOutFrame = channels[3];

            PostProcessingOfOutDiffFrame(MemOutFrame);

            // Dee Add
            // 查找轮廓并绘制轮廓
            Mat imgtmp;
            MemOutFrame.copyTo(imgtmp);
            vector<vector<Point>> contours;
            findContours(imgtmp, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

            // 查找最大框
            int max_i = -1;
            vector<Rect> boundRect(contours.size());
            Point tl_point,br_point;

            int size = 0; //存放框的大小值
            int x_temp=0;
            int y_temp=0;
            int width_temp=0;
            int height_temp=0;

            // 遍历列表，找出最大框
            for (int i = 0; i < contours.size(); i++){
                boundRect[i] = boundingRect(contours[i]);
                tl_point = boundRect[i].tl();
                br_point = boundRect[i].br();

                // 计算框的中心点坐标和宽高
                x_temp = (tl_point.x+br_point.x)/2;
                y_temp = (tl_point.y+br_point.y)/2;
                width_temp = br_point.x-tl_point.x+1;
                height_temp = br_point.y-tl_point.y+1;

                // 框的大小大于前一个就记录框的信息
                if (width_temp*height_temp > size){
                    size = width_temp*height_temp; // 更新框的大小值
                    max_i = i; // 记录最大框的index
                    pre_frame_info.x=x_temp;
                    pre_frame_info.y=y_temp;
                    pre_frame_info.w=width_temp;
                    pre_frame_info.h=height_temp;
                }
            }
            // 存在最大框，就画出红色框并传输图像到终端节点
            if(max_i >=0 ){
                DisplayFrame.copyTo(SendFrame);
                // 画红框
                rectangle(SendFrame, boundRect[max_i], Scalar(0,0,255), 2);
                // 对图像进行压缩
                int nsize = cvimg2sockdata2(SendFrame,encodeImg);
                // 通过udp传输图像到终端节点
                sendto(udp_sock_fd, encodeImg, nsize, 0, (struct sockaddr *)&udp_addr_serv, sizeof(udp_addr_serv));
                // 如果终端节点发送了start指令则记录结果帧中激光笔的位置信息和时刻信息
                if(g_flag_start){
                    g_frame_info_list[g_frame_index].x=pre_frame_info.x;
                    g_frame_info_list[g_frame_index].y=pre_frame_info.y;
                    g_frame_info_list[g_frame_index].w=pre_frame_info.w;
                    g_frame_info_list[g_frame_index].h=pre_frame_info.h;
                    g_frame_info_list[g_frame_index].time=int(pre_time);
                    g_frame_index++;
                }
            }else{ // 没有最大框，则传输原图像到终端节点
                DisplayFrame.copyTo(SendFrame);
                // 对图像进行压缩
                int nsize = cvimg2sockdata2(SendFrame,encodeImg);
                // 通过udp传输图像到终端节点
                sendto(udp_sock_fd, encodeImg, nsize, 0, (struct sockaddr *)&udp_addr_serv, sizeof(udp_addr_serv));
            }
        }

        // 更新pre_time和PreNum_w
        pre_time=cur_time;
        PreNum_w = CurNum_w;

        // 显示实时图像
        imshow("output", DisplayFrame);
        if ( waitKey(25) == 27 )
            break;
  }
  close(udp_sock_fd);
  return 0;
}