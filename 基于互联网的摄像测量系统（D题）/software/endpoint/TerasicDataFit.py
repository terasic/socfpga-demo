# !/usr/bin/env python3
#
##############################################################################
#
# Permission:
#
#   Terasic grants permission to use and modify this code for use
#   in synthesis for all Terasic Development Boards and Altera Development
#   Kits made by Terasic.  Other use of this code, including the selling
#   ,duplication, or modification of any portion is strictly prohibited.
#
# Disclaimer:
#
#   This VHDL/Verilog or C/C++/Python source code is intended as a design
#   reference which illustrates how these types of functions can be implemented.
#   It is the user's responsibility to verify their design for
#   consistency and functionality through the use of formal
#   verification methods.  Terasic provides no warranty regarding the use
#   or functionality of this code.
#
#  Terasic Technologies Inc
#  9F., No.176, Sec.2, Gongdao 5th Rd, East Dist, Hsinchu City, 30070. Taiwan
#
#                    web: http://www.terasic.com
#                    email: support@terasic.com
#
#   Author :  Dee Zeng
#   Date    :   2021/12/01
##############################################################################

"""
    This py provides a data process for object detect datas to a cos function:
        (A*np.cos(2*np.pi/T*x+phi)+offset)   (simply function constructed by Dee Zeng (target simple pendulum motion) )
        A: amplitude
        T: cycle
        phi:phase
        offset:offset

    input data： result of object detect ：  each frame contain a data struct :
        [left_top_x,
        left_top_y,
        width,
        height,
        time]

    process:  original -> interpolate -> leastsq -> target cos function

    Finally result:  length and amplitude of simple pendulum motion

"""

import numpy as np
from scipy.optimize import leastsq
import matplotlib.pyplot as plt
from scipy import interpolate
import scipy.signal as signal

print_level = 5  # 0 不打印， 1打印一点点，5 打印最详细

def debug_print(str, disable_print=False, self_print_level=4):
    if (self_print_level < print_level) and not disable_print:
        print(str)

####################################################
# 宏定义
data_interpolate_num = 500  # 插值数量控制

#####################################################
# 定义函数形式和误差
def target_func(t, p):
    A, T, phi, offset = p
    return A * np.cos(2*np.pi/T*t + phi) + offset

def residuals(p, y, x):
    return y - target_func(x, p)

class TerasicDataFit:

    def __init__(self, object_detect_data, gravity=9.8):
        self.data = object_detect_data
        self.gravity = gravity  # 重力参数校正传入

    def run(self):
        ##准备原始数据
        self.data_prepare()

        ##对原始数据进行插值
        self.data_interpolate()

        ##对插值数据进行 正弦波函数拟合
        self.data_fit()

        ## 结果处理和返回
        return self.data_result()

    def data_prepare(self):
        #####
        # function的因变量x：单摆运动的 中心点x坐标
        # x坐标 + 宽度/2 . 即中心点x坐标；
        self.original_x = [(data[0] + data[2] / 2) for data in self.data]

        #####
        # function的自变量t：时间轴
        t_list = [data[4] for data in self.data]
        self.original_t = []
        for i in range(len(t_list)):
            self.original_t.append(float(t_list[i] - t_list[0])/100000)  # 都减去初始值，时间轴从零时刻开始。

    def data_interpolate(self):
        self.interpolate_t = np.linspace(0, self.original_t[-1],
                                         data_interpolate_num)  # 0-最后一个时刻点，插值数量：data_interpolate_num
        func2 = interpolate.UnivariateSpline(self.original_t, self.original_x, s=8)
        self.interpolate_x = func2(self.interpolate_t)
        # 如果不插值，可以看看预测效果如何
        # self.interpolate_t = np.array(self.original_t)
        # self.interpolate_x = np.array(self.original_x)

    def data_fit(self):
        # 初始参数准备：
        fs = np.fft.fftfreq(len(self.interpolate_t), self.interpolate_t[1] - self.interpolate_t[0])
        Y = abs(np.fft.fft(self.interpolate_x))
        freq = abs(fs[np.argmax(Y[1:]) + 1])
        A = (max(self.interpolate_x) - min(self.interpolate_x)) / 2  # 振幅除以2
        T = 1/freq
        phi = 0
        offset = np.mean(self.interpolate_x)
        p0 = [A, T, phi, offset]
        # debug_print("初始参数值：" + str(p0))
        debug_print("original parameters: " + str(p0))

        # 拟合
        Para = leastsq(residuals, p0, args=(self.interpolate_x, self.interpolate_t))
        self.FuncPara = Para[0]
        debug_print("final parameters: " + str(self.FuncPara))

    def data_display(self):
        plt.figure(figsize=(8, 6))
        plt.scatter(self.original_t, self.original_x, color="green", label="Sample Point", linewidth=1)  # 画样本点
        # plt.scatter(self.interpolate_t,self.interpolate_x,color="blue",label="Interpolate Point",linewidth=1) #画样本点
        y_fit = target_func(self.interpolate_t, self.FuncPara)
        plt.plot(self.interpolate_t, y_fit, color="orange", label="Fitting Line", linewidth=2)  # 画拟合直线
        plt.legend()
        plt.show()

    def get_fit_data(self):
        y_fit = target_func(self.interpolate_t, self.FuncPara)
        return self.original_t,self.original_x,self.interpolate_t,y_fit

    def data_result(self):
        #### 单摆运动结果：
        # 周期 线长
        T = self.FuncPara[1]
        # T =2π√ (L / g)
        # L = (T/2π)^2*g
        length = self.gravity*(T*T/(4*np.pi*np.pi)) * 100 # 乘以100 换算为cm

        # x方向距离。  distantce_x  用于计算夹角
        distance_x = abs(2 * self.FuncPara[0])  # 两倍振幅参数

        return T, length, distance_x


if __name__ == "__main__":
    data_list = [[209, 137, 9, 21, 1637828132.17555], [216, 137, 8, 22, 1637828132.2555451],
                 [220, 142, 5, 12, 1637828132.3355978],
                 [226, 132, 5, 5, 1637828132.4957287], [219, 137, 9, 21, 1637828132.5756187],
                 [217, 137, 17, 22, 1637828132.6557386],
                 [211, 137, 19, 22, 1637828132.7356608], [206, 137, 16, 21, 1637828132.8156388],
                 [197, 137, 20, 22, 1637828132.895676],
                 [189, 136, 15, 21, 1637828132.9756784], [186, 137, 13, 20, 1637828133.0556908],
                 [183, 133, 6, 6, 1637828133.1356921],
                 [186, 136, 5, 14, 1637828133.3757503], [191, 136, 7, 21, 1637828133.4557557],
                 [189, 136, 9, 21, 1637828133.5357163],
                 [205, 137, 9, 21, 1637828133.6157815], [203, 137, 9, 21, 1637828133.6958144],
                 [210, 137, 9, 21, 1637828133.7758217],
                 [216, 137, 9, 21, 1637828133.8558216], [224, 138, 10, 9, 1637828133.9358866],
                 [219, 137, 9, 21, 1637828134.1758902],
                 [213, 137, 9, 21, 1637828134.2558978], [206, 137, 11, 21, 1637828134.3359108],
                 [206, 137, 16, 21, 1637828134.4159234],
                 [192, 136, 10, 21, 1637828134.495936], [189, 136, 15, 21, 1637828134.5759993],
                 [185, 136, 13, 21, 1637828134.6560857],
                 [183, 133, 6, 6, 1637828134.7359607], [182, 140, 5, 6, 1637828134.8960032],
                 [185, 137, 9, 19, 1637828134.9760163],
                 [188, 136, 15, 22, 1637828135.0560231], [194, 136, 17, 21, 1637828135.136051],
                 [206, 137, 9, 22, 1637828135.2160482], [204, 137, 10, 22, 1637828135.2960966],
                 [211, 137, 8, 22, 1637828135.37608], [217, 137, 8, 22, 1637828135.456401],
                 [218, 136, 8, 21, 1637828135.7761557], [212, 137, 9, 22, 1637828135.8561592],
                 [206, 137, 10, 21, 1637828135.9361815], [208, 137, 10, 21, 1637828136.016191],
                 [191, 136, 11, 21, 1637828136.0961647], [185, 136, 9, 20, 1637828136.1761878],
                 [188, 138, 8, 14, 1637828136.2562332], [187, 137, 9, 20, 1637828136.5762994],
                 [192, 137, 9, 20, 1637828136.6564496], [190, 136, 11, 21, 1637828136.736313],
                 [207, 137, 10, 21, 1637828136.8163292], [204, 137, 10, 21, 1637828136.8963642],
                 [211, 137, 9, 21, 1637828136.976366], [217, 137, 7, 22, 1637828137.0563781],
                 [226, 134, 5, 6, 1637828137.136464], [217, 137, 9, 22, 1637828137.376448],
                 [215, 137, 17, 22, 1637828137.4564562], [209, 137, 19, 22, 1637828137.5364163],
                 [205, 137, 14, 21, 1637828137.616431], [191, 136, 10, 21, 1637828137.6964762],
                 [189, 136, 16, 21, 1637828137.7765737], [185, 136, 12, 22, 1637828137.856529],
                 [182, 133, 5, 6, 1637828137.9365299], [182, 131, 5, 5, 1637828138.0965517],
                 [188, 137, 6, 20, 1637828138.176529], [185, 136, 8, 21, 1637828138.2565882],
                 [191, 136, 9, 21, 1637828138.336528], [207, 137, 9, 21, 1637828138.4167287],
                 [214, 137, 9, 21, 1637828138.4966218], [212, 137, 9, 21, 1637828138.5766776],
                 [218, 137, 8, 21, 1637828138.6567936], [217, 137, 9, 21, 1637828138.9767122],
                 [215, 137, 18, 22, 1637828139.0567913], [213, 137, 9, 21, 1637828139.136677],
                 [204, 137, 13, 21, 1637828139.2167044], [191, 136, 10, 21, 1637828139.296751],
                 [189, 136, 16, 22, 1637828139.3767862], [184, 138, 13, 17, 1637828139.4567454],
                 [182, 132, 5, 7, 1637828139.5367382], [184, 142, 6, 7, 1637828139.69683],
                 [189, 137, 6, 20, 1637828139.7768419], [186, 136, 8, 22, 1637828139.8568554],
                 [192, 136, 8, 21, 1637828139.936889], [208, 137, 8, 22, 1637828140.016889],
                 [205, 137, 9, 22, 1637828140.0968957], [212, 137, 9, 22, 1637828140.1768694],
                 [218, 137, 7, 22, 1637828140.2569294], [216, 137, 8, 22, 1637828140.5769925],
                 [210, 137, 9, 21, 1637828140.6571214], [204, 137, 10, 21, 1637828140.7369337],
                 [206, 137, 10, 21, 1637828140.8169782], [190, 136, 10, 21, 1637828140.8970366],
                 [192, 137, 9, 20, 1637828140.9770396], [189, 131, 6, 9, 1637828141.0570173],
                 [189, 137, 8, 20, 1637828141.3771157], [186, 136, 10, 20, 1637828141.4571652],
                 [192, 136, 10, 21, 1637828141.5370977], [209, 137, 9, 21, 1637828141.6171234],
                 [206, 137, 9, 21, 1637828141.6971722], [213, 137, 8, 21, 1637828141.7771533],
                 [218, 137, 6, 21, 1637828141.857197], [222, 138, 11, 17, 1637828142.0972393],
                 [216, 137, 9, 21, 1637828142.177214], [214, 137, 18, 22, 1637828142.2572901],
                 [207, 137, 19, 22, 1637828142.3372083], [205, 137, 10, 22, 1637828142.4172475],
                 [190, 136, 10, 21, 1637828142.4972763], [188, 136, 15, 22, 1637828142.577284],
                 [185, 132, 8, 8, 1637828142.6574054], [183, 143, 5, 6, 1637828142.7373483],
                 [184, 133, 6, 6, 1637828142.897338], [187, 138, 12, 18, 1637828142.977394],
                 [187, 136, 8, 21, 1637828143.0573657], [192, 137, 9, 20, 1637828143.137385],
                 [206, 137, 16, 21, 1637828143.2174373], [207, 137, 10, 21, 1637828143.2973995],
                 [213, 137, 9, 21, 1637828143.3774312], [219, 136, 8, 19, 1637828143.4574761],
                 [221, 140, 8, 12, 1637828143.6975155], [218, 137, 14, 22, 1637828143.7774858],
                 [209, 137, 11, 21, 1637828143.8575416], [211, 137, 9, 21, 1637828143.9375534],
                 [205, 137, 10, 21, 1637828144.017522], [188, 136, 9, 21, 1637828144.0975194],
                 [188, 136, 15, 21, 1637828144.177553], [185, 134, 8, 8, 1637828144.2575705],
                 [185, 143, 6, 5, 1637828144.4975758], [187, 136, 14, 21, 1637828144.5776036],
                 [188, 136, 8, 21, 1637828144.6577582], [193, 136, 9, 21, 1637828144.737638],
                 [210, 137, 8, 22, 1637828144.8177044], [207, 137, 9, 22, 1637828144.897667],
                 [214, 137, 8, 22, 1637828144.9776816], [219, 137, 6, 22, 1637828145.0578082],
                 [219, 138, 6, 17, 1637828145.2977195], [214, 137, 9, 22, 1637828145.3777547],
                 [217, 137, 9, 21, 1637828145.4578261], [211, 137, 9, 21, 1637828145.537781],
                 [204, 137, 10, 21, 1637828145.6178598], [188, 136, 10, 20, 1637828145.6977932],
                 [191, 137, 9, 20, 1637828145.777836]]

    DataFit = TerasicDataFit(data_list, gravity=9.8)
    T, length, distance_x = DataFit.run()
    debug_print("周期： " + str(T) + " s")
    debug_print("长度： " + str(length) + " cm")
    debug_print("x位移： " + str(distance_x) + " 像素点")

    DataFit.data_display()

