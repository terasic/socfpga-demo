from PyQt5 import QtWidgets
from PyQt5 import QtGui

import sys
import cv2
import socket
import time
import numpy

from threading import Thread
from TerasicDataFit import TerasicDataFit as tdf
from Ui_main import Ui_MainUi
from detail_ui import DetailWindow
from voice_light_system import VoiceLightSys

class main_ui(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.ui=Ui_MainUi()
        self.ui.setupUi(self)
        self.setup_ui_action()
        self.setup_ui_extra()

        self.voicelight=VoiceLightSys()

        self.init_env()
        self.print_info("init env ok")

    def setup_ui_action(self):
        self.ui.pushButton_start.clicked.connect(self.onclick_start_btn)
        self.ui.pushButton_show1.clicked.connect(self.onclick_show_camera1_data)
        self.ui.pushButton_show2.clicked.connect(self.onclick_show_camera2_data)

    def setup_ui_extra(self):
        self.start_btn=self.ui.pushButton_start
        self.line_label=self.ui.lineEdit_linelen
        self.angle_label=self.ui.lineEdit_angle
        self.cam1_label=self.ui.label_cam1
        self.cam2_label=self.ui.label_cam2
        self.info_edittxt=self.ui.textEdit_info

        self.cam1_label.setStyleSheet("border-width:1px;border-style:solid;border-color:black;")
        self.cam2_label.setStyleSheet("border-width:1px;border-style:solid;border-color:black;")
        self.line_label.setStyleSheet("border-width:1px;border-style:solid;border-color:black;color:blue;")
        self.angle_label.setStyleSheet("border-width:1px;border-style:solid;border-color:black;color:blue;")

        self.info_edittxt.setReadOnly(True)
        self.line_label.setReadOnly(True)
        self.angle_label.setReadOnly(True)

        self.ui.pushButton_show1.setDisabled(True)
        self.ui.pushButton_show2.setDisabled(True)

    def init_sockets(self,cam_A_ip,cam_B_ip,endpoint_ip):
        self.tcp1_addr = cam_A_ip
        self.tcp2_addr = cam_B_ip
        self.tcp1_port = 12345
        self.tcp2_port = 12346

        self.udp_addr = endpoint_ip
        self.udp1_port = 10000
        self.udp2_port = 10001

        Thread(target=self.deal_with_udp1,daemon=True).start()
        Thread(target=self.deal_with_udp2,daemon=True).start()
        Thread(target=self.recv_message1,daemon=True).start()
        Thread(target=self.recv_message2,daemon=True).start()
        Thread(target=self.deal_with_tcp,daemon=True).start()

        self.print_info("init sockets ok")

    def init_env(self):
        self.g_flag_runing=True
        self.L1=0
        self.L2=0
        self.X1=0
        self.X2=0
        self.T1=0
        self.T2=0
        self.g_x1_flag=False
        self.g_x2_flag=False
        self.g_start_time=0

        self.count_end_flag = True

        self.ox1 = None
        self.oy1 = None
        self.sx1 = None
        self.sy1 = None
        self.ox2 = None
        self.oy2 = None
        self.sx2 = None
        self.sy2 = None

        Thread(target=self.deal_with_result).start()

    def deal_with_tcp(self):
        while self.g_flag_runing:
            # 获取一次数据
            if self.g_start_time != 0 and ((time.time() - self.g_start_time) >= 10):
                self.tcp_s1.send("get_frame_data".encode('utf-8'))
                self.tcp_s2.send("get_frame_data".encode('utf-8'))
                self.g_start_time = 0
                self.print_info("tcp send get_frame_data to server")
            time.sleep(1)
            print("==========:", time.time() - self.g_start_time)

    def recvall(self,client, count):
        buf = b''
        while count:
            newbuf = client.recv(count)
            if not newbuf:
                return None
            buf += newbuf
            count -= len(newbuf)
        return buf

    def recv_message1(self):
        # tcp client 1
        tcp_addr1 = (self.tcp1_addr, self.tcp1_port)
        self.tcp_s1 = socket.create_connection(tcp_addr1)
        self.print_info("connect A tcp server ok!")
        while self.g_flag_runing:
            length = self.recvall(self.tcp_s1, 4)

            data_byte2 = int().from_bytes(length, byteorder='little', signed=True)
            print("length", data_byte2)
            stringData = self.recvall(self.tcp_s1, int(data_byte2))
            # print(stringData)

            struct_list = []
            for i in range(0, len(stringData) - 1, 20):
                tmp_list = []
                tmp_list.append(int().from_bytes(stringData[i:i + 4], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 4:i + 8], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 8:i + 12], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 12:i + 16], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 16:i + 20], byteorder='little', signed=True))
                struct_list.append(tmp_list)
            # print(struct_list)

            DataFit = tdf(struct_list, gravity=9.8)
            try:
                self.T1,self.L1, self.X1 = DataFit.run()
                self.ox1, self.oy1, self.sx1, self.sy1 = DataFit.get_fit_data()
            except:
                self.L1 = None

            if self.L1 == None:
                self.print_error("no enough data for A to compute L")
                self.start_btn.setEnabled(True)
            else:
                self.L1 = self.L1 - 5.165 # 减去激光笔的一半长度
                self.print_info("computing result for A...")
                self.g_x1_flag = True
                self.start_btn.setEnabled(True)

            # self.count_end_flag=True

    def recv_message2(self):
        # tcp client 2
        tcp_addr2 = (self.tcp2_addr, self.tcp2_port)
        self.tcp_s2 = socket.create_connection(tcp_addr2)
        self.print_info("connect B tcp server ok!")
        while self.g_flag_runing:
            length = self.recvall(self.tcp_s2, 4)

            data_byte2 = int().from_bytes(length, byteorder='little', signed=True)
            print("length", data_byte2)
            stringData = self.recvall(self.tcp_s2, int(data_byte2))
            # print(stringData)

            struct_list = []
            for i in range(0, len(stringData) - 1, 20):
                tmp_list = []
                tmp_list.append(int().from_bytes(stringData[i:i + 4], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 4:i + 8], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 8:i + 12], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 12:i + 16], byteorder='little', signed=True))
                tmp_list.append(int().from_bytes(stringData[i + 16:i + 20], byteorder='little', signed=True))
                struct_list.append(tmp_list)
            # print(struct_list)

            DataFit = tdf(struct_list, gravity=9.8)
            try:
                self.T2, self.L2, self.X2 = DataFit.run()
                self.ox2, self.oy2, self.sx2, self.sy2 = DataFit.get_fit_data()
            except:
                self.L2 = None

            if self.L2 == None:
                self.print_error("no enough data for B to compute L")
                self.start_btn.setEnabled(True)
            else:
                self.L2 = self.L2 - 5.165
                self.print_info("computing result for B...")
                self.g_x2_flag = True
                self.start_btn.setEnabled(True)

            # self.count_end_flag=True

    def deal_with_udp1(self):
        # udp server 1
        udp_addr1 = (self.udp_addr, self.udp1_port)
        udp_s1 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        udp_s1.bind(udp_addr1)
        self.print_info("A udp server started!")
        while True:
            start = time.time()
            data, addr = udp_s1.recvfrom(60000)
            if not data:
                break

            # show
            data_frame = numpy.frombuffer(data, numpy.uint8)
            decimg = cv2.imdecode(data_frame, cv2.IMREAD_COLOR)
            end = time.time()
            # fps
            seconds = end - start
            if seconds == 0:
                print("seconds == 0")
                pass
            else:
                fps = 1 / seconds
                font = cv2.FONT_HERSHEY_SIMPLEX
                decimg = cv2.putText(decimg, "fps:" + str(round(fps, 2)), (10, 25), font, 1, (0, 255, 255), 1)
            # 添加中心线
            decimg = cv2.line(decimg, (160, 0), (160, 240), (0, 255, 0), 1)

            showImage = cv2.cvtColor(decimg, cv2.COLOR_BGR2RGB)
            showImage = QtGui.QImage(showImage.data, showImage.shape[1], showImage.shape[0], QtGui.QImage.Format_RGB888)
            self.cam1_label.setPixmap(QtGui.QPixmap.fromImage(showImage))
        udp_s1.close()

    def deal_with_udp2(self):
        # udp server 2
        udp_addr2 = (self.udp_addr, self.udp2_port)
        udp_s2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        udp_s2.bind(udp_addr2)
        self.print_info("B udp server started!")
        while True:
            start = time.time()
            data, addr = udp_s2.recvfrom(60000)
            if not data:
                break

            # show
            data_frame = numpy.frombuffer(data, numpy.uint8)
            decimg = cv2.imdecode(data_frame, cv2.IMREAD_COLOR)
            end = time.time()
            # fps
            seconds = end - start
            if seconds == 0:
                print("seconds == 0")
                pass
            else:
                fps = 1 / seconds
                font = cv2.FONT_HERSHEY_SIMPLEX
                decimg = cv2.putText(decimg, "fps:" + str(round(fps, 2)), (10, 25), font, 1, (0, 255, 255), 1)
            # 添加中心线
            decimg = cv2.line(decimg, (160, 0), (160, 240), (0, 255, 0), 1)

            showImage = cv2.cvtColor(decimg, cv2.COLOR_BGR2RGB)
            showImage = QtGui.QImage(showImage.data, showImage.shape[1], showImage.shape[0], QtGui.QImage.Format_RGB888)
            self.cam2_label.setPixmap(QtGui.QPixmap.fromImage(showImage))
        udp_s2.close()

    def onclick_start_btn(self):
        self.g_start_time = time.time()
        self.tcp_s1.send("start".encode('utf-8'))
        self.tcp_s2.send("start".encode('utf-8'))
        self.print_info("start...")
        self.info_edittxt.setTextColor(QtGui.QColor(255, 0, 0))
        self.info_edittxt.append("0")
        self.count_end_flag = False
        self.start_btn.setDisabled(True)


    def onclick_show_camera1_data(self):
        detail_win_a = DetailWindow()
        detail_win_a.set_fig_data(self.ox1,self.oy1,self.sx1,self.sy1)
        detail_win_a.set_result_data(self.X1,self.T1,self.L1)
        detail_win_a.exec()

    def onclick_show_camera2_data(self):
        detail_win_a = DetailWindow()
        detail_win_a.set_fig_data(self.ox2, self.oy2, self.sx2, self.sy2)
        detail_win_a.set_result_data(self.X2, self.T2, self.L2)
        detail_win_a.exec()

    def deal_with_result(self):
        try:
            count = 1
            while True:
                time.sleep(1)
                if self.g_x2_flag and self.g_x1_flag:
                    self.g_x1_flag=False
                    self.g_x2_flag=False

                    if self.L1==None or self.L2==None:
                        pass
                    else:
                        if self.X1 >=self.X2:
                            length = str(round((self.L1), 2)) + " cm"
                            self.line_label.setText(length)
                            # self.angle_label.setText(str(self.X1))
                        else:
                            length = str(round((self.L2), 2)) + " cm"
                            self.line_label.setText(length)

                        angle=numpy.arctan(float(self.X1) / float(self.X2))*180/3.14
                        self.angle_label.setText(str(round(angle))+" °")

                        self.ui.pushButton_show1.setEnabled(True)
                        self.ui.pushButton_show2.setEnabled(True)

                    #声光系统启动
                    self.voicelight.buzzer_led_run()

                    # break
                    self.count_end_flag = True
                    count = 1

                elif self.count_end_flag==False:
                    self.info_edittxt.moveCursor(QtGui.QTextCursor.End)
                    lastline = self.info_edittxt.textCursor()
                    lastline.select(QtGui.QTextCursor.LineUnderCursor)
                    lastline.removeSelectedText()
                    self.info_edittxt.moveCursor(QtGui.QTextCursor.StartOfLine, QtGui.QTextCursor.MoveAnchor)

                    data = str(count).strip('\n')
                    self.info_edittxt.insertPlainText(data)
                    count += 1
        except:
            print("exit from deal with result")

    def print_info(self,info_data):
        self.info_edittxt.append("<font color=\"#0000FF\">[INFO] " +info_data +"</font>")
        self.info_edittxt.moveCursor(QtGui.QTextCursor.End)

    def print_error(self,info_data):
        self.info_edittxt.append("<font color=\"#FF0000\">[ERROR] " + info_data + "</font>")
        self.info_edittxt.moveCursor(QtGui.QTextCursor.End)

if __name__ == '__main__':
    if len(sys.argv) == 4:
        app = QtWidgets.QApplication(sys.argv)
        cam_A_ip = sys.argv[1]
        cam_B_ip = sys.argv[2]
        endpoint_ip = sys.argv[3]

        win = main_ui()

        win.init_sockets(cam_A_ip,cam_B_ip,endpoint_ip)
        win.show()

        sys.exit(app.exec_())

    print("Usage: python3 main_ui.py cam_A_ip cam_B_ip localhost_ip")

