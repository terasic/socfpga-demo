import os,mmap
from threading import Thread
import time

class VoiceLightSys:
    def __init__(self):
        self.mmap_init()
        self.init_config()

    def mmap_init(self):
        fd = os.open("/dev/mem",os.O_RDWR|os.O_SYNC)

        self.mem = mmap.mmap(fd,0x1000,mmap.MAP_SHARED,(mmap.PROT_READ | mmap.PROT_WRITE),mmap.ACCESS_WRITE,0xff200000)
        self.buzzer_led_addr = self.mem

        os.close(fd)

    def init_config(self):
        self.buzzer_led_off()

    def buzzer_led_on(self):
        self.buzzer_led_addr.seek(0)
        self.buzzer_led_addr.write_byte(3)

    def buzzer_led_off(self):
        self.buzzer_led_addr.seek(0)
        self.buzzer_led_addr.write_byte(0)

    def voice_light_3s(self):
        test.buzzer_led_on()
        time.sleep(3)
        test.buzzer_led_off()

    def buzzer_led_run(self):
        Thread(target=self.voice_light_3s).start()

if __name__ == '__main__':
    test = VoiceLightSys()
    while True:
        test.buzzer_led_on()
        time.sleep(1)
        test.buzzer_led_off()
        time.sleep(1)
