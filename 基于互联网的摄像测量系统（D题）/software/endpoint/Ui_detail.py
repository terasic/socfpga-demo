# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'detail.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DetailUi(object):
    def setupUi(self, DetailUi):
        DetailUi.setObjectName("DetailUi")
        DetailUi.resize(792, 387)
        DetailUi.setMinimumSize(QtCore.QSize(792, 387))
        DetailUi.setMaximumSize(QtCore.QSize(792, 387))
        self.lineEdit_T = QtWidgets.QLineEdit(DetailUi)
        self.lineEdit_T.setGeometry(QtCore.QRect(340, 350, 181, 21))
        self.lineEdit_T.setReadOnly(True)
        self.lineEdit_T.setObjectName("lineEdit_T")
        self.lineEdit_A = QtWidgets.QLineEdit(DetailUi)
        self.lineEdit_A.setGeometry(QtCore.QRect(90, 350, 171, 21))
        self.lineEdit_A.setReadOnly(True)
        self.lineEdit_A.setObjectName("lineEdit_A")
        self.label_3 = QtWidgets.QLabel(DetailUi)
        self.label_3.setGeometry(QtCore.QRect(10, 350, 101, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(DetailUi)
        self.label_4.setGeometry(QtCore.QRect(280, 350, 72, 15))
        self.label_4.setObjectName("label_4")
        self.lineEdit_L = QtWidgets.QLineEdit(DetailUi)
        self.lineEdit_L.setGeometry(QtCore.QRect(600, 350, 181, 21))
        self.lineEdit_L.setReadOnly(True)
        self.lineEdit_L.setObjectName("lineEdit_L")
        self.label_5 = QtWidgets.QLabel(DetailUi)
        self.label_5.setGeometry(QtCore.QRect(540, 350, 72, 15))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(DetailUi)
        self.label_6.setGeometry(QtCore.QRect(10, 10, 101, 16))
        self.label_6.setObjectName("label_6")
        self.widget_canva = QtWidgets.QWidget(DetailUi)
        self.widget_canva.setGeometry(QtCore.QRect(10, 30, 771, 311))
        self.widget_canva.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.widget_canva.setObjectName("widget_canva")

        self.retranslateUi(DetailUi)
        QtCore.QMetaObject.connectSlotsByName(DetailUi)

    def retranslateUi(self, DetailUi):
        _translate = QtCore.QCoreApplication.translate
        DetailUi.setWindowTitle(_translate("DetailUi", "CameraData"))
        self.label_3.setText(_translate("DetailUi", "Amplitude:"))
        self.label_4.setText(_translate("DetailUi", "Period:"))
        self.label_5.setText(_translate("DetailUi", "Length:"))
        self.label_6.setText(_translate("DetailUi", "Graphic："))
